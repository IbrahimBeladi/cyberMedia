new Vue({
	mixins: [vuetifyMixin],
	el: "#app",
	router,
	i18n,
	mounted: function () {
		var x = getCookie("lang");
		if (x) {
			i18n.locale = x;
			document.documentElement.lang = x;
			this.$vuetify.rtl = (x == "ar");
		} else {
			var defaultLang = "en";
			setCookie("lang", defaultLang);
			i18n.locale = defaultLang;
			document.documentElement.lang = defaultLang;
			this.$vuetify.rtl = false;
		}
	},
	computed: {
		dir: function () {
			if (i18n.locale == 'ar') {
				return "rtl";
			} else {
				return "ltr";
			}
		}
	}
});