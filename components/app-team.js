Vue.component("app-team", {
	template: `
		<v-card class="py-2 fill-height">
			<v-card-title class="justify-center pt-12">
				<span class="headline">
					{{ $t("team.team") }}
				</span>
			</v-card-title>
			<v-card-text class="pa-1">
				<v-container :class="getClass">
					<v-row dense justify="center">
						<v-col cols="12" sm="6" md="4" v-for="p in team" :key="p.name">
							<v-card class="primary">
								<v-card-text class="text-center py-2">
									<span class="title">
										{{ $t("team."+p.name) }}
									</span>
									<p class="subheading ma-0">
										- {{ $t("team."+p.name+"2") }}
									</p>
								</v-card-text>
							</v-card>
						</v-col>
					</v-row>
				</v-container>
			</v-card-text>
		</v-card>
	`,
	data: function () {
		return {
			team: [
				{ name: "ibra" },
				{ name: "osama" },
				{ name: "azoz" },
				{ name: "abood" },
				{ name: "musab" },
			]
		}
	},
	computed: {
		height: function () {
			switch (this.$vuetify.breakpoint.name) {
				case "xs": return "50px";
				case "sm": return "100px";
				case "md": return "150px";
				case "lg": return "200px";
				case "xl": return "250px";
			}
		},
		getClass: function () {
			switch (this.$vuetify.breakpoint.name) {
				case "xs": return "pa-0";
				case "sm": return "pa-1";
				case "md": return "pa-2";
				case "lg": return "pa-4";
				case "xl": return "pa-6";
			}
		}
	}
});