Vue.component("app-media", {
	template: `
		<v-card class="py-2 fill-height">
			<v-card-title class="justify-center pt-12">
				<span class="headline">
					{{ $t( "links.title1" ) }}
				</span>
			</v-card-title>
			<v-card-text class="pa-1">
				<v-container grid-list-md :class="getClass">
					<v-row dense justify="center">
						<v-col cols="12" sm="6" md="4" v-for="link in links" :key="link.title" class="media-item">
							<a target="_blank" style="text-decoration: none;" @click="test(link)">
								<v-card class="text-center pb-6 pt-12 primary">
									<v-card-media class="center-icon pt-4 pb-2">
										<v-icon size="100">
											{{ link.icon }}
										</v-icon>
									</v-card-media>
									<v-card-text class="pa-4">
										<span class="body-1">
											{{ $t( link.code ) }}
										</span>
									</v-card-text>
								</v-card>
							</a>
						</v-col>
					</v-row>	
				</v-container>
			</v-card-text>
		</v-card>
	`,
	data: function () {
		return {
			links: [
				{ code: "media.crimeLaws", icon: "mdi-security", both: "./res/pdf/Anti-Cyber Crime Law.pdf" },
				{ code: "media.presentation", icon: "mdi-presentation-play", both: "./res/presentation/CyberMedia.exe" },
				{ code: "media.infographic", icon: "mdi-camcorder-box", ar: "./res/videos/cyberMedia - Arabic.mp4", en: "./res/videos/cyberMedia - English.mp4" },
			]
		}
	},
	computed: {
		getClass: function () {
			switch (this.$vuetify.breakpoint.name) {
				case "xs": return "pa-0";
				case "sm": return "pa-1";
				case "md": return "pa-2";
				case "lg": return "pa-4";
				case "xl": return "pa-6";
			}
		}
	},
	methods: {
		test: function (link) {
			if (link.both) {
				open(link.both, "_blank")
			} else {
				if (this.$i18n.locale == "ar") {
					open(link.ar, "_blank")
				} else {
					open(link.en, "_blank")
				}
			}
		}
	}
});