Vue.component("app-hero-section", {
    template: `
        <v-parallax src="./res/imgs/parallax.jpg" :height="parallaxHeight">
            <v-carousel delimiter-icon="mdi-stop" interval="7000" :show-arrows="false" :height="parallaxHeight">
                <v-carousel-item v-for="(item,i) in 2" :key="i">
                    <v-row class="fill-height ma-0">
                        <v-row align="center" class="pa-0">
                            <v-col cols="12">
                                <v-card dark flat style="background-color: rgba(0,0,0,0.8);">
                                    <v-card-text class="text-center white--text">
                                        <span :class="quoteSize">
                                            "{{ $t("message.quote"+(i+1)) }}"
                                        </span>
                                        <span :class="quoterSize" v-html="$t('message.quoter')"></span>
                                    </v-card-text>
                                </v-card>
                            </v-col>
                        </v-row>
                    </v-row>
                </v-carousel-item>
            </v-carousel>
        </v-parallax>
    `,
    computed: {
        parallaxHeight: function () {
            switch (this.$vuetify.breakpoint.name) {
                case "xs": return "300";
                case "sm": return "350";
                case "md": return "400";
                case "lg": return "500";
                case "xl": return "500";
            }
        },
        quoteSize: function(){
            switch (this.$vuetify.breakpoint.name) {
                case "xs": return "heading larger";
                case "sm": return "display-1";
                case "md": return "display-2";
                case "lg": return "display-3";
                case "xl": return "display-3";
            }
        },
        quoterSize: function(){
            switch (this.$vuetify.breakpoint.name) {
                case "xs": return "subheading";
                case "sm": return "heading";
                case "md": return "display-1";
                case "lg": return "display-1";
                case "xl": return "display-1";
            }
        }
    }
});