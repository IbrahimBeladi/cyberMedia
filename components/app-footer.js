Vue.component("app-footer", {
    template: `
        <v-footer :color="color" dark height="auto">
            <v-row justify="center" dense>
                <v-col cols="12" class="text-center pa-0">
                    <v-btn text rounded dark href="https://ibrahimbeladi.sa" target="_blank" class="ma-0" style="text-transform: none;">
                        Beladi © {{ new Date().getFullYear() }}
                    </v-btn>
                </v-col>
            </v-row>
        </v-footer>
    `,
    props: {
        color: {
            default: "primary"
        }
    }
})