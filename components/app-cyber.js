Vue.component("app-cyber", {
	template: `
		<v-card class="py-12">
			<v-container :class="getClass">
				<v-expansion-panels accordion :value="0">
					<v-expansion-panel :key="0" expand-icon="mdi-chevron-down" class="primary darken-2">
						<v-expansion-panel-header class="title">
							{{ $t( "cyber.cybersecurity" ) }}
                        </v-expansion-panel-header>
                        <v-expansion-panel-content>
                            <v-container grid-list-md :class="getClass">
                                <v-row dense justify="center">
                                    <v-col cols="12" v-for="question,i in cyberQuestions" :key="i">
                                        <v-card class="elevation-2">
                                            <v-card-title class="pb-0">
                                                <span class="title">
                                                    {{ $t( "cyber.cyber.q"+(i+1) ) }}
                                                </span>
                                            </v-card-title>
                                            <v-card-text>
                                                <span class="subheading">
                                                    {{ $t( "cyber.cyber.a"+(i+1) ) }}
                                                </span>
                                            </v-card-text>
                                        </v-card>
                                    </v-col>
                                </v-row>	
                            </v-container>
                        </v-expansion-panel-content>
					</v-expansion-panel>
					<v-expansion-panel :key="1" expand-icon="mdi-chevron-down" class="primary darken-2">
						<v-expansion-panel-header class="title">
							{{ $t( "cyber.types" ) }}
                        </v-expansion-panel-header>
                        <v-expansion-panel-content>
                            <v-container grid-list-md :class="getClass">
                                <v-row dense justify="center">
                                    <v-col cols="12" v-for="question,i in typeQuestions" :key="i">
                                        <v-card class="elevation-2">
                                            <v-card-title class="pb-0">
                                                <span class="title">
                                                    {{ $t( "cyber.type.t"+(i+1) ) }}
                                                </span>
                                            </v-card-title>
                                            <v-card-text>
                                                <span class="subheading">
                                                    {{ $t( "cyber.type.d"+(i+1) ) }}
                                                </span>
                                            </v-card-text>
                                        </v-card>
                                    </v-col>
                                </v-row>	
                            </v-container>
                        </v-expansion-panel-content>
					</v-expansion-panel>
				</v-expansion-panels>
			</v-container>	
		</v-card>
	`,
	data: function () {
		return {
			cyberQuestions: 4,
			typeQuestions: 4,
		}
	},
	computed: {
		getClass: function () {
			switch (this.$vuetify.breakpoint.name) {
				case "xs": return "pa-0";
				case "sm": return "pa-0";
				case "md": return "pa-1";
				case "lg": return "pa-1";
				case "xl": return "pa-2";
			}
		}
	}
});