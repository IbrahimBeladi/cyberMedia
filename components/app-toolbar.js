Vue.component("app-toolbar", {
	template: `
	<div>
		<v-app-bar :color="color" dense app dark>
			<v-toolbar-items>
                <v-btn text to="home">
                    <v-avatar>
                        <img src="./res/imgs/logo.png"/>
                    </v-avatar>
                </v-btn>
				<template v-for="item,i in items">
					<v-btn text :to="item.to" class="hidden-xs-only">
						{{ $t( item.title ) }}
					</v-btn>
				</template>
			</v-toolbar-items>

			<v-spacer></v-spacer>

            <v-toolbar-items>
				<v-btn text @click="changeLang" class="hidden-xs-only">
					{{ isEN ? "عربي" : "English" }}
				</v-btn>

				<v-app-bar-nav-icon @click="drawer = true" class="hidden-sm-and-up">
					<v-icon>mdi-menu</v-icon>
				</v-app-bar-nav-icon>
			</v-toolbar-items>			
		</v-app-bar>

		<v-navigation-drawer dark temporary app v-model="drawer" :right="isEN" class="hidden-sm-and-up">
			<v-list dense rounded>
				<v-list-item to="/home">
					<v-list-item-icon>
						<v-icon>
							mdi-home
						</v-icon>
					</v-list-item-icon>
					<v-list-item-content>
						<v-list-item-title>
							{{ $t( "links.home" ) }}
						</v-list-item-title>
					</v-list-item-content>
				</v-list-item>
			</v-list>
			
			<v-divider></v-divider>

			<v-list dense rounded>
				<v-list-item v-for="item in items" :key="item.title" :to="item.to">
					<v-list-item-icon>
						<v-icon>
							{{ item.icon }}
						</v-icon>
					</v-list-item-icon>
					<v-list-item-content>
						<v-list-item-title>
							{{ $t( item.title ) }}
						</v-list-item-title>
					</v-list-item-content>
				</v-list-item>
			</v-list>

			<v-divider></v-divider>

			<v-list dense rounded>
				<v-list-item @click="changeLang">
					<v-list-item-icon>
						<v-icon>
							mdi-translate
						</v-icon>
					</v-list-item-icon>
					<v-list-item-content>
						<v-list-item-title>
							{{ isEN ? "عربي" : "English" }}
						</v-list-item-title>
					</v-list-item-content>
				</v-list-item>
			</v-list>
		</v-navigation-drawer>
	</div>
	`,
	data: () => ({
		drawer: false,
		items: [
			{ to: "media", title: "links.title1", icon: "mdi-video" },
			{ to: "team", title: "links.title2", icon:"mdi-account-group"},
		]
	}),
	props: {
		color: {
			type: String,
			default: "primary"
		}
	},
	methods: {
		changeLang() {
			if (i18n.locale == "en") {
				setCookie("lang", "ar");
				i18n.locale = "ar";
                document.documentElement.lang = "ar";
                this.$vuetify.rtl = true;
			}
			else {
				setCookie("lang", "en");
				i18n.locale = "en";
                document.documentElement.lang = "en";
                this.$vuetify.rtl = false;
			}
		}
	},
	computed: {
		isEN: function () {
			return i18n.locale == "en";
		}
	}
});