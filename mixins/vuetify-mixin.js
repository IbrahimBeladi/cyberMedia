var vuetifyMixin = {
    vuetify: new Vuetify({
        theme: {
            themes: {
                light: {
                    primary: '#616161',
				},
				dark: {
                    primary: '#616161',
                }
			},
			dark: true
        },
    }),
}